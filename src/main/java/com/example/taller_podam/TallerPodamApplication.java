package com.example.taller_podam;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TallerPodamApplication {

	public static void main(String[] args) {
		SpringApplication.run(TallerPodamApplication.class, args);
	}
}
