package com.example.taller_podam;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class Phone {
   String manufacturer;
   String model;
   Integer releaseYear;
   Integer usedTime;

   public void useForAYear() {
       usedTime = usedTime + 1;
   }
}