package com.example.taller_podam;

import uk.co.jemos.podam.api.AttributeMetadata;
import uk.co.jemos.podam.api.DataProviderStrategy;
import uk.co.jemos.podam.typeManufacturers.StringTypeManufacturerImpl;
import java.lang.reflect.Type;
import java.util.Map;


public class CustomStringPhone extends StringTypeManufacturerImpl {

       @Override
       public String getType(DataProviderStrategy strategy,
                       AttributeMetadata attributeMetadata,
                       Map<String, Type> genericTypesArgumentsMap) {

               if (Phone.class.isAssignableFrom(attributeMetadata.getPojoClass())) {

                       if ("manufacturer".equals(attributeMetadata.getAttributeName())) {
                               return PhoneTestConstants.MANUFACTURER;
                       } else if ("model".equals(attributeMetadata.getAttributeName())) {
                               return PhoneTestConstants.MODEL;
                       }
               }
               return super.getType(strategy, attributeMetadata, genericTypesArgumentsMap);
       };
}