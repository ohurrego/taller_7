package com.example.taller_podam;

public class PhoneTestConstants {

   private PhoneTestConstants () {}

   public static final String MANUFACTURER = "Google";

   public static final String MODEL = "Pixel XL";

   public static final Integer RELEASE_YEAR = 2017;

   public static final Integer USED_TIME = 0;
}