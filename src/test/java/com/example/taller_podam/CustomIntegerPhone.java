package com.example.taller_podam;

import uk.co.jemos.podam.api.AttributeMetadata;
import uk.co.jemos.podam.api.DataProviderStrategy;
import uk.co.jemos.podam.typeManufacturers.IntTypeManufacturerImpl;

import java.lang.reflect.Type;
import java.util.Map;


public class CustomIntegerPhone extends IntTypeManufacturerImpl {

       @Override
       public Integer getType(DataProviderStrategy strategy,
                       AttributeMetadata attributeMetadata,
                       Map<String, Type> genericTypesArgumentsMap) {

               if (Phone.class.isAssignableFrom(attributeMetadata.getPojoClass())) {

                       if ("releaseYear".equals(attributeMetadata.getAttributeName())) {
                               return PhoneTestConstants.RELEASE_YEAR;
                       } else if ("usedTime".equals(attributeMetadata.getAttributeName())) {
                               return PhoneTestConstants.USED_TIME;
                       }
               }
               return super.getType(strategy, attributeMetadata, genericTypesArgumentsMap);
       };
}