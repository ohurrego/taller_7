package com.example.taller_podam;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import uk.co.jemos.podam.api.PodamFactory;
import uk.co.jemos.podam.api.PodamFactoryImpl;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TallerPodamApplicationTests {

	private static PodamFactory factory = new PodamFactoryImpl();


	@BeforeClass
	public static void init() {

		CustomStringPhone customStringManufacturer = new CustomStringPhone();
		CustomIntegerPhone customIntegerPhone = new CustomIntegerPhone();

		factory.getStrategy().addOrReplaceTypeManufacturer(String.class, customStringManufacturer);
		factory.getStrategy().addOrReplaceTypeManufacturer(Integer.class, customIntegerPhone);

	}

	@Test
	public void testPhoneUsage() {


		Phone phone = factory.manufacturePojoWithFullData(Phone.class);

		System.out.println(phone);
		Assert.assertNotNull("The phone cannot be null!", phone);
		Assert.assertNotNull("The manufacturer of the phone cannot be null!",
		phone.getManufacturer());
		Assert.assertEquals("The manufacturer is not the expected", phone.getManufacturer(), PhoneTestConstants.MANUFACTURER);
		Assert.assertEquals("The model is not the expected", phone.getModel(), PhoneTestConstants.MODEL);
		Integer expectedUsedTime = PhoneTestConstants.USED_TIME + 1;
		phone.useForAYear();
		Assert.assertEquals("The phone didn’t get used", expectedUsedTime, phone.getUsedTime());

	}

}
