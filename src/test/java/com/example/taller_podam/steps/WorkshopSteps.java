package com.example.taller_podam.steps;

import com.example.taller_podam.CustomIntegerPhone;
import com.example.taller_podam.CustomStringPhone;
import com.example.taller_podam.Phone;
import com.example.taller_podam.PhoneTestConstants;
import net.thucydides.core.annotations.Step;
import org.junit.Assert;
import uk.co.jemos.podam.api.DataProviderStrategy;
import uk.co.jemos.podam.api.PodamFactory;
import uk.co.jemos.podam.api.PodamFactoryImpl;

public class WorkshopSteps {

    private static PodamFactory factory = new PodamFactoryImpl();

    @Step("Given factory string and factory integer")
    public void addOrReplaceSpecific() {
        CustomStringPhone customStringManufacturer = new CustomStringPhone();
        CustomIntegerPhone customIntegerPhone = new CustomIntegerPhone();

        factory.getStrategy().addOrReplaceTypeManufacturer(String.class, customStringManufacturer);
        factory.getStrategy().addOrReplaceTypeManufacturer(Integer.class, customIntegerPhone);
    }

    @Step("When the phone is used for 1 year")
    public void createPhonePojo(){


    }

    @Step("Then the time of use is 1 year")
    public void validateUserYear(){
        Phone phone = factory.manufacturePojoWithFullData(Phone.class);
        System.out.println(phone);
        Assert.assertNotNull("The phone cannot be null!", phone);
        Assert.assertNotNull("The manufacturer of the phone cannot be null!",
                phone.getManufacturer());
        Assert.assertEquals("The manufacturer is not the expected", phone.getManufacturer(), PhoneTestConstants.MANUFACTURER);
        Assert.assertEquals("The model is not the expected", phone.getModel(), PhoneTestConstants.MODEL);
        phone.useForAYear();
        Integer expectedUsedTime = PhoneTestConstants.USED_TIME + 1;
        Assert.assertEquals("The phone didn’t get used", expectedUsedTime, phone.getUsedTime());
    }

}
