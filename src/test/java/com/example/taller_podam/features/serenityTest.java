package com.example.taller_podam.features;


import com.example.taller_podam.steps.WorkshopSteps;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Title;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(SerenityRunner.class)
public class serenityTest {

    @Test
    @Title("Podam should handle immutable POJOs annotated with @PodamConstructor")
    public void podamShouldHandleImmutablePojosAnnotatedWithPodamConstructor() throws Exception {

        WorkshopSteps workshopSteps = new WorkshopSteps();

        workshopSteps.addOrReplaceSpecific();
        workshopSteps.createPhonePojo();
        workshopSteps.validateUserYear();

    }

}
